# Trabalho de Estrutura de Dados 2

## Instruções

### Linha de Comando

#### Compilar o projeto

- Entre na pasta do projeto pelo terminal
- Execute os comandos:

```sh
javac -d . src/ds2/**/*.java
jar cvmf META-INF/MANIFEST.MF ed2.jar ds2
```

> Caso apareça a mensagem abaixo apenas ignore

```
Note: Some input files use unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
```

#### Executar

- Após ter compilado o projeto, ainda dentro da pasta do projeto, pelo terminal execute:

```sh
java -jar ed2.jar
```

- Irá abrir uma interface solicitando o arquivo `brazil_cities_coordinates.csv`
- Após outra solicitando o arquivo `brazil_covid19_cities_processado.csv`

> Para padronizar os nossos arquivos `.csv`, definimos o caractere separador do arquivo processado, como sendo a vírgula
> antes estava especificamente pra esse arquivo o ponto e vírgula.
> Dessa forma, obtenha no link abaixo o arquivo com o separador atualizado.

[Link arquivo processado](https://u.pcloud.link/publink/show?code=XZn74BXZ5cKKjQMu3kkcS3dlbLp8x4Flqlvk)

> Está disponível no arquivo `brazil_covid19_cities_processado-demo.csv` um pequeno número de registros para testes rápidos

#### Executável Pré-compilado

Release v1.0:

- [Link](https://gitlab.com/btd1337/ed2-t2/-/releases/v1.0)
