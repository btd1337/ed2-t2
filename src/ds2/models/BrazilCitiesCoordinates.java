package ds2.models;

import ds2.utils.CSVReader;

import java.text.ParseException;
import java.util.List;
import java.util.Objects;

public class BrazilCitiesCoordinates implements CSVReader<BrazilCitiesCoordinates> {
	private String stateCode;
	private String cityCode;
	private String cityName;
	private Double latitude;
	private Double longitude;
	private boolean isCapital;

	public BrazilCitiesCoordinates() {}

	public BrazilCitiesCoordinates(String stateCode, String cityCode, String cityName, Double latitude, Double longitude, boolean isCapital) {
		this.stateCode = stateCode;
		this.cityCode = cityCode;
		this.cityName = cityName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.isCapital = isCapital;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public boolean isCapital() {
		return isCapital;
	}

	public void setCapital(boolean capital) {
		isCapital = capital;
	}

	@Override
	public String toString() {
		return "[" +
				"stateCode='" + stateCode + '\'' +
				", cityCode='" + cityCode + '\'' +
				", cityName='" + cityName + '\'' +
				", latitude=" + latitude +
				", longitude=" + longitude +
				", isCapital=" + isCapital +
				"]";
	}

	@Override
	public BrazilCitiesCoordinates getFromCSV(List<String> csvLine) throws ParseException {
		String stateCode = csvLine.get(0);
		String cityCode = csvLine.get(1);
		String cityName = csvLine.get(2);
		Double latitude = Double.valueOf(csvLine.get(3));
		Double longitude = Double.valueOf(csvLine.get(4));
		boolean isCapital = (csvLine.get(5)) == "true" ? true : false;

		return new BrazilCitiesCoordinates(stateCode, cityCode, cityName, latitude, longitude, isCapital);
	}

	@Override
	public String toCSV() {
		return this.stateCode + ";" + this.cityCode + ";" + this.cityName + ";" + this.latitude.toString() + ";" + this.longitude.toString() + ";" + (this.isCapital == true ? "TRUE" : "FALSE");
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BrazilCitiesCoordinates that = (BrazilCitiesCoordinates) o;
		return isCapital == that.isCapital && stateCode.equals(that.stateCode) && cityCode.equals(that.cityCode) && cityName.equals(that.cityName) && latitude.equals(that.latitude) && longitude.equals(that.longitude);
	}

	@Override
	public int hashCode() {
		return Objects.hash(stateCode, cityCode, cityName, latitude, longitude, isCapital);
	}
}
