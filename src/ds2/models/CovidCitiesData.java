package ds2.models;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import ds2.utils.CSVReader;
import ds2.utils.Handlers;

public class CovidCitiesData implements CSVReader<CovidCitiesData> {

	private String name;

	private String state;

	private Float code;

	private Date date;

	private Integer cases;

	private Integer deaths;

	public CovidCitiesData() {}

	public CovidCitiesData(String name, String state, Float code, Date date, Integer cases, Integer deaths) throws ParseException {
		super();
		this.name = name;
		this.state = state;
		this.code = code;
		this.date = date;
		this.cases = cases;
		this.deaths = deaths;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Float getCode() {
		return code;
	}

	public void setCode(Float code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getCases() {
		return cases;
	}

	public void setCases(Integer cases) {
		this.cases = cases;
	}

	public Integer getDeaths() {
		return deaths;
	}

	public void setDeaths(Integer deaths) {
		this.deaths = deaths;
	}

	@Override
	public String toString() {
		return "[name=" + name + ", state=" + state + ", code=" + code + ", date=" + Handlers.dateToString(date) + ", cases="
				+ cases + ", deaths=" + deaths + "]";
	}

	public String toCSV() {
		return state + "," + name + "," + Handlers.dateToString(date) + "," + code + "," + cases + "," + deaths;
	}

	@Override
	public CovidCitiesData getFromCSV(List<String> csvLine) throws ParseException {
		String state = csvLine.get(0);
		String name = csvLine.get(1);
		Date date = Handlers.stringToDate(csvLine.get(2), "dd/MM/yyyy");
		Float code = Float.valueOf(csvLine.get(3));
		Integer cases = Integer.valueOf(csvLine.get(4));
		Integer deaths = Integer.valueOf((csvLine.get(5)));

		return new CovidCitiesData(name,state,code,date,cases,deaths);
	}

	public static Comparator<CovidCitiesData> compareByStateAndCityThenDate() {
		return (CovidCitiesData a, CovidCitiesData b) -> {
			int stateCompare = a.getState().compareTo(b.getState());
			if (stateCompare != 0) {
				return stateCompare;
			} else {
				int cityCompare = a.getName().compareTo(b.getName());
				if (cityCompare != 0) {
					return cityCompare;
				} else {
					return a.getDate().compareTo(b.getDate());
				}
			}
		};
	}

	public static Comparator<CovidCitiesData> compareByTotalCases() {
		return (CovidCitiesData a, CovidCitiesData b) -> a.getCases().compareTo(b.getCases());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CovidCitiesData that = (CovidCitiesData) o;
		return name.equals(that.name) && state.equals(that.state) && code.equals(that.code) && date.equals(that.date) && cases.equals(that.cases) && deaths.equals(that.deaths);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, state, code, date, cases, deaths);
	}
}

