package ds2.utils;

import java.time.Duration;
import java.time.Instant;

public class Comparator {
    private int comparisons;
    private Duration processTime;
    private Instant startTime;
    private Instant endTime;

    public Comparator() {
        this.comparisons = 0;
        this.startTime = Instant.now();
    }

    public int getComparisons() {
        return comparisons;
    }

    public String getProcessTime() {
        return processTime.toString().replace("PT", "");
    }

    public void addComparisons() {
        this.comparisons++;
    }

    public void finishComparation() {
        this.endTime = Instant.now();
        this.processTime = Duration.between(this.startTime, this.endTime);
    }

    @Override
    public String toString() {
        return "Comparator [Comparações de valores: " + this.getComparisons() + ", Tempo de Processamento: " + this.getProcessTime()
                + "]";
    }

}
