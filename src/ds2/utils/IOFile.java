package ds2.utils;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IOFile {
	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';

	/**
	 * Obtém um arquivo através de uma interface gráfica
	 *
	 * @return arquivo selecionado pelo usuário
	 * @throws FileNotFoundException
	 */
	public static File getFileFromDialog() throws FileNotFoundException {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int result = fileChooser.showOpenDialog(null);

		if (result != JFileChooser.APPROVE_OPTION) {
			throw new FileNotFoundException();
		}

		return fileChooser.getSelectedFile();
	}

	/**
	 * Método que lê um arquivo CSV retorna os dados em forma de lista
	 *
	 * @param path:      caminho para o arquivo
	 * @param hasHeader: informa se o arquivo tem cabeçalho
	 * @param usedClass: classe do tipo de dado que será a lista
	 * @return um lista de objetos do tipo usedClass
	 * @throws FileNotFoundException
	 */
	public static <T extends CSVReader<T>> ArrayList<T> readObjectsFromCSV(String path, boolean hasHeader, Class<T> usedClass) throws FileNotFoundException {
		ArrayList<T> objects = new ArrayList<T>();
		Scanner scanner = new Scanner(new File(path));

		if (hasHeader) {
			scanner.nextLine();
		}

		while (scanner.hasNext()) {
			List<String> line = parseLine(scanner.nextLine());

			try {
				objects.add(usedClass.getConstructor().newInstance().getFromCSV(line));
			} catch (NumberFormatException e) {
				System.err.println("Erro: Id com formato inválido! \n" + e);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		scanner.close();

		return objects;
	}

	public static <T extends CSVReader<T>> ArrayList<T> getRecordsFromFile(String fileName, Class<T> usedClass) throws FileNotFoundException {
		return IOFile.getRecordsFromFile(fileName, usedClass, true);
	}

	public static <T extends CSVReader<T>> ArrayList<T> getRecordsFromFile(String fileName, Class<T> usedClass, boolean hasHeader) throws FileNotFoundException {
		File inputFile;
		System.out.printf("Por favor, selecione o arquivo %s\n", fileName);

		inputFile = IOFile.getFileFromDialog();
		System.out.printf("Lendo arquivo %s...\n\n", inputFile.getName());
		return IOFile.readObjectsFromCSV(inputFile.getAbsolutePath(), hasHeader, usedClass);
	}

	/**
	 * Escreve os dados de uma lista de objetos em um arquivo CSV
	 *
	 * @param items
	 * @param fileName
	 * @param isAppend
	 * @param <T>
	 * @throws IOException
	 */
	public static <T extends CSVReader<T>> void writeObjectsToCSV(List<T> items, String fileName, boolean isAppend) throws IOException {
		// Criar arquivo de saída
		String outputFile = fileName + ".csv";
		File file = new File(outputFile);
		FileWriter fileWriter;
		Scanner scanner = new Scanner(System.in);

		try {
			fileWriter = new FileWriter(file, isAppend);
			PrintWriter printWriter = new PrintWriter(fileWriter);

			for (T item :
					items) {
				printWriter.printf(item.toCSV() + "\n");
			}
			printWriter.close();
			System.out.printf("\n! Os dados foram escritos com sucesso no arquivo %s\n\n", outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escreve os dados de uma lista de objetos em um arquivo CSV
	 * Por padrão utiliza a opção append como falso
	 *
	 * @param items
	 * @param fileName
	 * @param <T>
	 * @throws IOException
	 */
	public static <T extends CSVReader<T>> void writeObjectsToCSV(List<T> items, String fileName) throws IOException {
		IOFile.writeObjectsToCSV(items, fileName, false);
	}

	/*
	 * parseLine: Métodos de tratamento de linhas CSV
	 */
	private static List<String> parseLine(String csvLine) {
		return parseLine(csvLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
	}

	private static List<String> parseLine(String cvsLine, char separators, char customQuote) {

		List<String> result = new ArrayList<>();

		// Se vazio, retornar!
		if (cvsLine == null || cvsLine.isEmpty()) {
			return result;
		}

		if (customQuote == ' ') {
			customQuote = DEFAULT_QUOTE;
		}

		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}

		StringBuffer curVal = new StringBuffer();
		boolean inQuotes = false;
		boolean startCollectChar = false;
		boolean doubleQuotesInColumn = false;

		char[] chars = cvsLine.toCharArray();

		for (char ch : chars) {

			if (inQuotes) {
				startCollectChar = true;
				if (ch == customQuote) {
					inQuotes = false;
					doubleQuotesInColumn = false;
				} else {

					// Corrigido: Permitir "" dentro da string
					if (ch == '\"') {
						if (!doubleQuotesInColumn) {
							curVal.append(ch);
							doubleQuotesInColumn = true;
						}
					} else {
						curVal.append(ch);
					}

				}
			} else {
				if (ch == customQuote) {

					inQuotes = true;

					// Fixed : permitir "" entre aspas vazias
					if (chars[0] != '"' && customQuote == '\"') {
						curVal.append('"');
					}

					// tratar aspas duplas
					if (startCollectChar) {
						curVal.append('"');
					}

				} else if (ch == separators) {

					result.add(curVal.toString());

					curVal = new StringBuffer();
					startCollectChar = false;

				} else if (ch == '\r') {
					// Ignorar caracteres LF
					continue;
				} else if (ch == '\n') {
					// Final da linha
					break;
				} else {
					curVal.append(ch);
				}
			}

		}

		result.add(curVal.toString());

		return result;
	}

	public static void writeDataInFile(String outputFile, String data, boolean isAppend) throws IOException {
		File file = new File(outputFile);

		if (!file.exists()) {
			file.createNewFile();
			isAppend = false;
		}
		FileWriter fileWriter = new FileWriter(file, isAppend);
		PrintWriter printWriter = new PrintWriter(fileWriter);

		printWriter.printf(data);
		printWriter.printf(System.lineSeparator() + System.lineSeparator());
		printWriter.close();
	}
}

