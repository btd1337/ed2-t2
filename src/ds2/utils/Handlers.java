package ds2.utils;

import ds2.structures.hash.Hash;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Handlers {

	public static Integer doubleToInteger(double doubleValue) {
		return Integer.valueOf((int) doubleValue);
	}

	public static Date stringToDate(String date, String format) throws ParseException {
		Date formattedDate = new SimpleDateFormat(format).parse(date);

		return formattedDate;
	}

	public static Date stringToDate(String date) throws ParseException {
		return Handlers.stringToDate(date, "yyyy-MM-dd");
	}

	public static String dateToString(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		return dateFormat.format(date);
	}

	/*
	 * Este método sorteia um sub-conjunto dentro de uma lista de entrada
	 *
	 * Parâmetros:
	 *
	 * books: Lista de elementos que serão sorteados
	 * size: Número de elementos a serem sorteados
	 * seed: Uma semente a ser utilizada no gerador de números aleatórios
	 *
	 * Retorno: Um subconjunto da lista de entrada
	 *
	 */
	public static <T> ArrayList<T> getRandomSubset(List<T> items, int size, int seed) {
		ArrayList<T> selectedItems = new ArrayList<T>();
		ArrayList<Integer> indexes = Handlers.getRandomNumbersArray(size, 0, items.size()-1, seed);

		for (Integer element:
				indexes) {
			selectedItems.add(items.get(element));
		}

		return selectedItems;
	}

	public static boolean isPrimeNumber(long number) {
		for (long i = 2; i < number; i++) {
			if (number % i == 0)
				return false;
		}
		return true;
	}

	/**
	 * Gera uma tabela hash de registros através de uma lista
	 * @param elements lista de elementos
	 * @param <T> tipo da classe
	 */
	public static <T> Hash<T> generateHashTableFromList(List<T> elements) {
		int tableSize = Hash.recommendedHashTableSize(elements.size());
		Hash<T> hashTable = new Hash<T>(tableSize, Hash.division);

		for (T object: elements) {
			hashTable.insert(object.hashCode(), object);
		}

		return hashTable;
	}

	/**
	 * Verificador do próximo número primo
	 * @param number: número a ser verificado
	 * @return o número primo subsequente a um número informado
	 */
	public static long getNextPrimeNumber(long number) {
		long value = number;
		boolean isPrimeNumber = false;

		do {
			isPrimeNumber = Handlers.isPrimeNumber(value);

			if (!isPrimeNumber) {
				value += 1;
			}
		} while(!isPrimeNumber);
		return value;
	}

	public static ArrayList<Integer> getRandomNumbersArray(int numElements, int min, int max, int seed) {
		Random randomIndex = new Random(seed);
		Set<Integer> indexes = new HashSet<Integer>();
		ArrayList<Integer> generatatedNumbers = new ArrayList<>();

		if (min > max) {
			int aux = min;
			min = max;
			max = aux;
		}

		// A utilização da estrutura Set garante que items repetidos não sejam adicionados
		while (indexes.size() < numElements) {
			indexes.add((randomIndex.nextInt(max - min + 1) + min));
		}

		for (int index : indexes) {
			generatatedNumbers.add(index);
		}

		return generatatedNumbers;
	}

	public static float getStatTime(Stats stats){
		String aux = stats.getProcessTime().replace("S", "");
		if(aux.contains("M")){
			String before = aux.split("M")[0];
			String after = aux.substring(aux.lastIndexOf("M") + 1);
			Float begin = Float.parseFloat(before) * 60;
			Float end = Float.parseFloat(after);
			return begin + end;
		} else{
			return Float.parseFloat(aux);
		}
	}

	public static int medianOfThreeRandomized(int lowestIndex, int highestIndex, int seed) {
		ArrayList<Integer> indexes = Handlers.getRandomNumbersArray(3, lowestIndex, highestIndex, seed);
		return Math.max(Math.min(indexes.get(0), indexes.get(1)), Math.min(Math.max(indexes.get(0), indexes.get(1)), indexes.get(2)));
	}
}

