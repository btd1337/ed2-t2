package ds2.utils;

import java.text.ParseException;
import java.util.List;

public interface CSVReader<T> {
	public T getFromCSV(List<String> csvLine) throws ParseException;

	public String toCSV();
}
