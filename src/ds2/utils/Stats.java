package ds2.utils;
import java.time.Duration;
import java.time.Instant;

/*
 * Dica de utilização da classe:
 *
 * Criar um objeto Stats no início do algoritmo sorteador
 * Stats comparator = new Stats();
 *
 * Registrar trocas e comparações através dos métodos getNumComparations e addNumSwaps
 * comparator.getNumComparations()
 * comparator.addNumSwaps()
 *
 * Finalizar o temporizador ao término do algoritmo de ordenação
 * comparator.finishComparation()
 *
 * Obter os dados estatísticos através do método toString()
 * comparator.toString()
 */

public class Stats {
	private long numSwaps;
	private long numComparations;
	private Duration processTime;
	private Instant startTime;
	private Instant endTime;

	public Stats() {
		this.numSwaps = 0L;
		this.numComparations = 0L;
		this.startTime = Instant.now();
	}

	public long getNumSwaps() {
		return numSwaps;
	}

	public long getNumComparations() {
		return numComparations;
	}

	public String getProcessTime() {
		return processTime.toString().replace("PT", "");
	}

	public void addNumComparations() {
		this.numComparations++;
	}

	public void addNumSwaps() {
		this.numSwaps++;
	}

	public void finishComparation() {
		this.endTime = Instant.now();
		this.processTime = Duration.between(this.startTime, this.endTime);
	}

	@Override
	public String toString() {
		return "Stats [numComparations=" + numComparations + ", numSwaps=" + numSwaps + ", processTime="
				+ this.getProcessTime() + "]";
	}

}

