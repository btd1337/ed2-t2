package ds2;

import ds2.models.BrazilCitiesCoordinates;
import ds2.models.CovidCitiesData;
import ds2.structures.btree.BTree;
import ds2.structures.hash.Hash;
import ds2.structures.pair.Pair;
import ds2.structures.quadtree.Node;
import ds2.structures.quadtree.QuadTree;
import ds2.structures.avltree.AVLTree;
import ds2.structures.tree.GenericNode;
import ds2.structures.tree.GenericTree;
import ds2.utils.Comparator;
import ds2.utils.Handlers;
import ds2.utils.IOFile;

import java.io.*;
import java.text.ParseException;
import java.util.*;

public class Main {

	public static void main(String[] args) throws IOException {
		int bTreeOrder = 4;
		int bTree2Order = 50;

		String brazilCitiesCoordinatesFileName = "brazil_cities_coordinates.csv";
		String covidCitiesFileName = "brazil_covid19_cities_processado.csv";

		ArrayList<BrazilCitiesCoordinates> brazilCitiesCoordinatesData;
		ArrayList<CovidCitiesData> covidCitiesData;

		Hash<CovidCitiesData> hashTable;

		QuadTree quadTree = new QuadTree();
		AVLTree avlTree = new AVLTree();
		BTree bTreeSmallOrder = new BTree(bTreeOrder);
		BTree bTreeBigOrder = new BTree(bTree2Order);

		System.out.println("=== TRABALHO DE ED2 - Parte 2 ===");

		try {
			brazilCitiesCoordinatesData = IOFile.getRecordsFromFile(brazilCitiesCoordinatesFileName, BrazilCitiesCoordinates.class);
			covidCitiesData = IOFile.getRecordsFromFile(covidCitiesFileName, CovidCitiesData.class, false);
		} catch (FileNotFoundException e) {
			System.out.println("Erro: Arquivo requerido não encontrado!");
			return ;
		}

		System.out.printf("Registros do arquivo: %s\n", brazilCitiesCoordinatesFileName);
		for (BrazilCitiesCoordinates bcc: brazilCitiesCoordinatesData) {
			quadTree.insert(new Node(bcc));
		}

		int tableSize = Hash.recommendedHashTableSize(covidCitiesData.size());
		hashTable = new Hash<CovidCitiesData>(tableSize, Hash.division);

		System.out.println("Inserindo dados na tabela hash");
		Comparator comparator = new Comparator();
		for (CovidCitiesData object: covidCitiesData) {
			Pair<String, Date> key = new Pair<String, Date>(object.getCode().toString(), object.getDate());
			hashTable.insert(key.hashCode(), object);
		}
		comparator.finishComparation();
		System.out.printf("Tempo de processamento: %s | Número de colisões: %s\n\n", comparator.getProcessTime(), hashTable.getCollisions());

		Main.testModule(covidCitiesData, brazilCitiesCoordinatesData);

		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe qual árvore deseja analisar as buscas");
		System.out.printf("1- AVL\n2- BTree com ordem pequena\n3- BTree com ordem grande\n");
		int option = scanner.nextInt();
		switch (option) {
			case 1: Main.analyzeStructures(hashTable, covidCitiesData, avlTree);
			case 2: Main.analyzeStructures(hashTable, covidCitiesData, bTreeSmallOrder);
			case 3: Main.analyzeStructures(hashTable, covidCitiesData, bTreeBigOrder);
			default: {
				System.out.println("Opção inválida!");
				return;
			}
		}
	}

	public static <U> void testModule(List<CovidCitiesData> elementsA, List<U> elementsB) {
		int option;
		int numElements = 0;
		Scanner scanner = new Scanner(System.in);
		do {
			System.out.printf("\nEscolha os testes que deseja executar:\n");
			System.out.println("1- QuadTree");
			System.out.println("2- Hash");
			System.out.println("3- Árvore AVL");
			System.out.println("4- Árvore B");
			System.out.println("0- Sair");

			try {
				option = scanner.nextInt();
			} catch (InputMismatchException e) {
				scanner.next();
				option = -1;
			}

			if (option >= 1 && option <= 4) {
				System.out.printf("\nInforme o número de elementos (n):\n");

				try {
					numElements = scanner.nextInt();
				} catch (InputMismatchException e) {
					scanner.next();
					numElements = -1;
				}

			}

			switch (option) {
				case 1:
					Main.runQuadTreeTests(elementsB, numElements);
					break;
				case 2:
					Main.runHashTests(elementsA, numElements);
					break;
				case 3:
					Main.runAVLTests(elementsA, numElements);
					break;
				case 4:
					Main.runBTreeTests(elementsA, numElements);
					break;
				case 0:
					System.out.printf("\nSaindo...\n");
					return;
				default:
					System.out.printf("Opção inválida\n\n");
					break;
			}
		} while (option != 0);
	}

	public static <T> void runQuadTreeTests(List<T> elements, int numElements) {
		QuadTree quadTree = new QuadTree();
		int seed = readSeed();
		String header = "== Testes Quad Tree: " + numElements + " elementos, semente: " + seed + " ===";
		List<T> elementsSelected = Handlers.getRandomSubset(elements, numElements, seed);
		for(T elt: elementsSelected){
			quadTree.insert(new Node((BrazilCitiesCoordinates) elt));
		}
		if(numElements <= 20) {
			System.out.println("QuadTree: " + numElements);
			quadTree.print();
		} else {
			String outputFile = "saida-quadtree.txt";
			String data = header + System.lineSeparator();
			quadTree.updateListOfFields();
			for(int i = 0; i < quadTree.getFields().size() - 1; i++){
				data += quadTree.getFields().get(i) + System.lineSeparator();
			}
			try {
				IOFile.writeDataInFile(outputFile, data, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void runHashTests(List<CovidCitiesData> elements, int numElements) {
		String outputFile = "saida-hash.txt";
		int seed = readSeed();

		String header = "== Testes Hash: " + numElements + " elementos, semente: " + seed + " ===";
		// exibe aviso caso o número de elementos informado seja maior que a lista
		if (numElements > elements.size()) {
			System.out.printf("\nAtenção: A lista contém apenas %d registro(s)\n\n", elements.size());
			numElements = elements.size();
		}

		// cria uma lista auxiliar com numElementos aleatórios da lista elements
		List<CovidCitiesData> elementsSelected = Handlers.getRandomSubset(elements, numElements, seed);

		// identifica o tamanho recomendado para a tabela hash
		int tableSize = Hash.recommendedHashTableSize(numElements);

		Hash<CovidCitiesData> hashTable = new Hash<CovidCitiesData>(tableSize, Hash.division);

		for (CovidCitiesData element: elementsSelected) {
			Pair<String, Date> key = new Pair<String, Date>(element.getCode().toString(), element.getDate());
			hashTable.insert(key.hashCode(), element);
		}

		if (numElements > 20) {
			String data = header + System.lineSeparator();
			data += hashTable.printHash(true);
			try {
				IOFile.writeDataInFile(outputFile, data, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println(header);
			System.out.printf(hashTable.printHash(true));
		}
	}

	public static <T> void runAVLTests(List<T> elements, int numElements) {
		String outputFile = "saida-avl.txt";
		int seed = readSeed();

		String header = "== Testes AVL: " + numElements + " elementos, semente: " + seed + " ===";

		// exibe aviso caso o número de elementos informado seja maior que a lista
		if (numElements > elements.size()) {
			System.out.printf("\nAtenção: A lista contém apenas %d registro(s)\n\n", elements.size());
			numElements = elements.size();
		}

		// cria uma lista auxiliar com numElementos aleatórios da lista elements
		List<T> elementsSelected = Handlers.getRandomSubset(elements, numElements, seed);
		AVLTree avlTree = new AVLTree();

		for (T element: elementsSelected) {
			avlTree.insert(element.hashCode());
		}

		if (numElements > 20) {
			String data = header + System.lineSeparator();
			data += AVLTree.printAVL(avlTree);
			try {
				IOFile.writeDataInFile(outputFile, data, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println(header);
			System.out.printf(AVLTree.printAVL(avlTree));
		}
	}

	public static <T> void runBTreeTests(List<T> elements, int numElements) {
		BTree bTree = new BTree(4);
		int seed = readSeed();
		List<T> elementsSelected = Handlers.getRandomSubset(elements, numElements, seed);
		String header = "== Testes BTree: " + numElements + " elementos" + seed + " ===";
		for(T elt: elementsSelected){
			bTree.insert(elt.hashCode());
		}
		if(numElements <= 20) {
			System.out.println("BTree: " + numElements);
			bTree.print();
		} else {
			String outputFile = "saida-btree.txt";
			String data = header + System.lineSeparator();
			bTree.updateListOfValues();
			for(int i = 0; i < bTree.getValues().size() - 1; i++){
				data += bTree.getValues().get(i).toString() + System.lineSeparator();
			}
			try {
				IOFile.writeDataInFile(outputFile, data, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static <T,U extends GenericNode> void analyzeStructures(Hash<T> hashTable, List<CovidCitiesData> elements, GenericTree<U> tree) throws IOException {
		int nValues[] = {10000, 50000, 100000, 500000, 1000000};
		// int nValues[] = {44, 50, 100, 500, 1000}; // for small tests
		int option;
		Scanner scanner = new Scanner(System.in);

		int seed = readSeed();

		for (int i=0; i < nValues.length; i++) {

			// seleciona N elementos aleatórios para inserir na árvore
			List<CovidCitiesData> elementsSelected = new ArrayList<CovidCitiesData>();
			ArrayList<Integer> indexes = Handlers.getRandomNumbersArray(nValues[i], 0, elements.size()-1, seed);
			for (Integer element:indexes) {
				elementsSelected.add(elements.get(element));
			}

			// insere chaves na árvore
			for (CovidCitiesData element: elementsSelected) {
				Pair<String,Date> key = new Pair<>(element.getCode().toString(), element.getDate());
				tree.insert(key.hashCode());
			}

			System.out.println("Informe a busca que deseja realizar");
			System.out.printf("1- S1\n2- S2 (*Não implementada)\n");
			option = scanner.nextInt();
			switch (option) {
				case 1: searchS1(hashTable, tree);
				case 2: searchS2(hashTable, elements);
				default: {
					System.out.println("Opção inválida!");
					return;
				}
			}
		}

	}

	public static <T, U extends GenericNode> void searchS1(Hash<T> hashTable, GenericTree<U> tree) throws IOException {
		int elementToSearch = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("Informe o código da cidade (Arquivo brazil_cities_coordinates):");
		String cityCode = scan.nextLine();
		cityCode = cityCode.substring(0, cityCode.length() - 1);
		cityCode += ".0";
		System.out.println("Informe a data (dd/MM/yyyy):");
		String date = scan.nextLine();
		try {
			elementToSearch = new Pair(cityCode, Handlers.stringToDate(date, "dd/MM/yyyy")).hashCode();
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}
		String data = "";
		Comparator comparator = new Comparator();

		U node = tree.search(elementToSearch, comparator);
		if(node == null){
			System.out.println("Registro não encontrado na árvore!");
			return;
		}
		CovidCitiesData ccd = (CovidCitiesData) hashTable.find(node.getValue());
		if(ccd == null){
			System.out.println("Registro não encontrado na tabela hash!");
			return;
		}
		data += "Número de casos da cidade " + ccd.getName() + " na data " + date + ": " + ccd.getCases() + System.lineSeparator() + comparator.toString() + System.lineSeparator();
		System.out.println(data);
		IOFile.writeDataInFile("saida.txt", data,true);
	}

	public static <T> void searchS2(Hash<T> hashTable, List<CovidCitiesData> elements) {

	}

	public static int readSeed() {
		int seed;
		Scanner scanner = new Scanner(System.in);
		do {
			try {
				System.out.println("\n=> Informe um valor, inteiro positivo, para a semente: ");
				seed = scanner.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("\n! Erro: valor deve ser um número inteiro positivo!\n");
				scanner.next();
				seed = -1;
			}
		} while (seed < 1);

		return seed;
	}
}
