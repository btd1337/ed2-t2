package ds2.structures.avltree;

import ds2.structures.tree.GenericTree;
import ds2.utils.Comparator;

import java.util.LinkedList;
import java.util.Queue;

public class AVLTree implements GenericTree<Node> {

	private Node root;

	public Node search(int value, Comparator comparator) {
		Node current = root;
		while (current != null) {
			if (current.getValue() == value) {
				break;
			}
			comparator.addComparisons();
			current = current.getValue() < value ? current.getRight() : current.getLeft();
		}
		comparator.finishComparation();
		return current;
	}

	public Node getRoot() {
		return this.root;
	}

	public int higher() {
		return this.root == null ? -1 : this.root.getHigher();
	}

	public void insert(int id) {
		this.root = insert(this.root, id);
	}

	private Node insert(Node node, int id) {
		if (node == null) {
			return new Node(id);
		} else {
			if (node.getValue() > id) {
				node.setLeft(insert(node.getLeft(), id));
			} else if (node.getValue() < id) {
				node.setRight(insert(node.getRight(), id));
			} else {
				throw new RuntimeException("Error: Duplicate Key");
			}
		}
		return rebalance(node);

	}

	private Node rebalance(Node node) {
		updateHigher(node);
		int balance = balancingFactor(node);
		if (balance > 1) {
			if (higher(node.getRight().getRight()) > higher(node.getRight().getLeft())) {
				node = rotateLeft(node);
			} else {
				node.setRight(rotateRight(node.getRight()));
				node = rotateLeft(node);
			}
		} else if (balance < -1) {
			if (higher(node.getLeft().getLeft()) > higher(node.getLeft().getRight())) {
				node = rotateRight(node);
			} else {
				node.setLeft(rotateLeft(node.getLeft()));
				node = rotateRight(node);
			}
		}
		return node;
	}

	private int higher(Node node) {
		return node == null ? -1 : node.getHigher();
	}

	public int balancingFactor(Node node) {
		return (node == null) ? 0 : higher(node.getRight()) - higher(node.getLeft());
	}

	private Node rotateRight(Node p) {
		Node q = p.getLeft();
		Node r = q.getRight();
		q.setRight(p);
		p.setLeft(r);
		updateHigher(p);
		updateHigher(q);
		return q;
	}

	private Node rotateLeft(Node p) {
		Node q = p.getRight();
		Node r = q.getLeft();
		q.setLeft(p);
		p.setRight(r);
		updateHigher(p);
		updateHigher(q);
		return q;
	}

	private void updateHigher(Node node) {
		node.setHigher(1 + Math.max(higher(node.getLeft()), higher(node.getRight())));
	}

	public static String printAVL(AVLTree avl) {
		String data = "";

		Node root = avl.root;
		// Caso base
		if (root == null) {
			return "";
		}

		// Cria uma fila vazia para passagem de ordem de nível
		Queue<Node> queue = new LinkedList<Node>();

		// Enfileirar a raiz e inicializar a altura
		queue.add(root);


		while (true) {

			// nodeCount (tamanho da fila) indica o número de nós
			// no nível atual
			int nodeCount = queue.size();
			if (nodeCount == 0) {
				break;
			}

			// Retirar da fila todos os nós do nível atual e enfileirar todos
			// os nós do próximo nível
			while (nodeCount > 0) {
				Node node = queue.peek();
				data += node.getValue() + " ";
				queue.remove();
				if (node.getLeft() != null) {
					queue.add(node.getLeft());
				}
				if (node.getRight() != null) {
					queue.add(node.getRight());
				}
				nodeCount--;
			}
			data += System.lineSeparator();
			;
		}

		return data;
	}
}
