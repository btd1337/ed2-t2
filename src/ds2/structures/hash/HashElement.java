package ds2.structures.hash;

public class HashElement<T> {
	private Integer key;
	private T data;

	public HashElement(Integer key, T data) {
		this.key = key;
		this.data = data;
	}

	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public T getData() {
		return this.data;
	}
	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "[key=" + key.toString() + ", data=" + data.toString() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HashElement<T> other = (HashElement<T>) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (key != other.key)
			return false;
		return true;
	}
}
