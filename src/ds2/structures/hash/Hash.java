package ds2.structures.hash;

import ds2.utils.Handlers;

import java.util.ArrayList;
import java.util.function.BiFunction;

public class Hash<T> {
	private ArrayList<HashElement<T>>[] hashTable;
	private int tableSize;
	private int numberKeys;
	private float loadFactor;
	private BiFunction<Integer, Integer,Integer> hash;
	private long collisions;
	private static final int minInitialCapacity = 17;
	public static final float defaultLoadFactor = 0.75f;
	// margem de espaço que irá ficar livre para novas inserções
	// após fazer a definição entre o número de elementos e o tamanho da tabela
	private static float marginForExpansion = 0.1f;

	public static BiFunction <Integer, Integer, Integer> division = (key, tableSize) -> (int) (Math.abs(key.hashCode()) % tableSize);
	public static BiFunction <Integer, Integer, Integer> multiplication = (key, tableSize) -> Handlers.doubleToInteger(Math.floor(tableSize*(Math.abs(key.hashCode())*((Math.sqrt(5)-1)/2)%1)));

	@SuppressWarnings("unchecked")
	public Hash(int tableSize, BiFunction<Integer, Integer,Integer> hash) {

		// Valida o tamanho mínimo para a tabela hash
		if (tableSize >= minInitialCapacity) {
			this.tableSize = tableSize;
		} else {
			System.out.println("O tamanho mínimo para a tabela hash é " + minInitialCapacity);
			this.tableSize = minInitialCapacity;
			System.out.println("Novo tamanho da tabela é: " + this.tableSize);
		}
		this.numberKeys = 0;
		this.hash = hash;
		this.collisions = 0;
		this.loadFactor = 0;

		this.hashTable = new ArrayList[this.tableSize];

		for(int i=0; i<this.tableSize; i++) {
			this.hashTable[i] = new ArrayList<HashElement<T>>();
		}
	}

	public HashElement<T> insert(Integer key, T data) {
		int tableIndex = this.getHashTableIndex(key);
		if(this.hashTable[tableIndex].size() > 1) {
			this.collisions ++;
		}
		this.hashTable[tableIndex].add(new HashElement<T>(key, data));
		int lastElementIndex = this.hashTable[tableIndex].size() - 1;
		this.numberKeys += 1;
		this.updateLoadFactor();
		return this.hashTable[tableIndex].get(lastElementIndex);
	}

	// Pesquisa se data com chave key está na tabela
	// Se estiver, retorna ponteiro para data, caso contrário retorna null
	public HashElement<T> lookup( Integer key, T data) {
		int tableIndex = this.getHashTableIndex(key);
		int index = this.hashTable[tableIndex].indexOf(new HashElement<T>(key, data));
		return index != -1 ? this.hashTable[tableIndex].get(index) : null;
	}

	// Pesquisa se a chave key está na tabela
	// Se estiver, retorna ponteiro contendo o elemento, caso contrário retorna null
	public T find(Integer key) {
		int tableIndex = this.getHashTableIndex(key);

		for (int i=0; i < this.hashTable[tableIndex].size(); i++) {
			if (this.hashTable[tableIndex].get(i).getKey().equals(key)) {
				return this.hashTable[tableIndex].get(i).getData();
			}
		}
		return null;
	}

	public long getCollisions() {
		return this.collisions;
	}

	public long getTableSize() {
		return this.tableSize;
	}

	// Retorna os elementos da tabela hash em formato de lista
	public ArrayList<T> getValues() {
		ArrayList<T> values = new ArrayList<T>();

		for (ArrayList<HashElement<T>> elements: this.hashTable) {
			for (HashElement<T> element: elements) {
				values.add(element.getData());
			}
		}

		return values;
	}

	public void destroy() {
		for(int i=0; i < this.hashTable.length; i++) {
			this.hashTable[i].clear();
		}
	}

	// Obtém o índice de um elemento através de uma chave
	public int getHashTableIndex( Integer key) {
		return this.hash.apply(key, this.tableSize);
	}

	private void updateLoadFactor() {
		this.loadFactor = this.numberKeys/this.tableSize;

		if (this.loadFactor > defaultLoadFactor) {
			this.rehashing();
		}
	}

	/**
	 * Cálcula o tamanho recomendado para uma tabela hash
	 * Utiliza como bom fator de carga valores próximos a 0.75
	 * Deixa um espaço de 10% para novas inserções após a tabela ter sido criada
	 * Retorna o número primo subsequente a esse valor
	 * @param numberKeys: Número de chaves inicial da tabela
	 */
	public static int recommendedHashTableSize(int numberKeys) {
		int value = (int) Math.ceil((numberKeys * ( 1 + marginForExpansion )) / defaultLoadFactor);
		int tableSize = (int) Handlers.getNextPrimeNumber(value);
		return tableSize > minInitialCapacity ? tableSize : minInitialCapacity;
	}


	private void rehashing() {
		// TODO Criar método que amplia o tamanho da tabela hash
	}

	public String printHash(boolean isFull) {
		String data = "";
		for(int i=0; i<this.tableSize; i++) {
			data += "[" + i + "," + this.hashTable[i].size() + "]: ";

			if (isFull) {
				for(int j=0; j < this.hashTable[i].size(); j++) {
					data += j == this.hashTable[i].size() - 1 ? this.hashTable[i].get(j).toString() : this.hashTable[i].get(j).toString() + ", ";
				}
			}
			// caractere de quebra de linha baseado no sistema operacional
			data += System.lineSeparator();
		}

		return data;
	}
}
