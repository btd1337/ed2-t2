package ds2.structures.tree;

public interface GenericNode {
    public int getValue();
}
