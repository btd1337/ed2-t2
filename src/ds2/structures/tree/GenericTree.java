package ds2.structures.tree;

import ds2.structures.avltree.Node;
import ds2.utils.Comparator;

import java.text.ParseException;
import java.util.List;

public interface GenericTree<T> {
	public void insert(int key);

	public T search(int value, Comparator comparator);
}
