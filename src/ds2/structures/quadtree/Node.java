package ds2.structures.quadtree;

import ds2.models.BrazilCitiesCoordinates;

public class Node {

    private String field;
    private double x;
    private double y;
    private Node[] children;
    private boolean capital;
    private BrazilCitiesCoordinates bcc;

    public Node(BrazilCitiesCoordinates bcc) {
        this.field = bcc.getCityName();
        this.x = bcc.getLongitude();
        this.y = bcc.getLatitude();
        this.children = new Node[4];
        this.capital = bcc.isCapital();
        this.bcc = bcc;
    }

    public String getField() {
        return field;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Node getSingleChild(int position) {
        return this.children[position];
    }

    public Node[] getChildren() {
        return children;
    }

    public void setChild(int position, Node node) {
        this.children[position] = node;
    }

    public boolean isCapital() {
        return capital;
    }

    public BrazilCitiesCoordinates getBcc() { return bcc; }

}
