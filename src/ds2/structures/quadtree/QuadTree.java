package ds2.structures.quadtree;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class QuadTree {

    private Node root;
    private ArrayList<Integer> cities;
    private ArrayList<String> fields;

    public void insert(Node node) {
        if(this.root == null) {
            this.root = node;
            return;
        }

        Node current = this.root;
        Node parent = this.root;
        int position = -1;

        while(current != null) {
            parent = current;
            if(node.getX() < current.getX()) {
                if(node.getY() < current.getY()) {
                    current = current.getSingleChild(2);
                    position = 2;
                    //Ir para sudoeste
                } else {
                    current = current.getSingleChild(1);
                    position = 1;
                    //Ir para noroeste
                }
            } else {
                if(node.getY() < current.getY()) {
                    current = current.getSingleChild(3);
                    position = 3;
                    //Ir para sudeste
                } else {
                    current = current.getSingleChild(0);
                    position = 0;
                    //Ir para nordeste
                }
            }
        }

        parent.setChild(position, node);
    }

    public Node search(int x, int y) {
        Node current = this.root;
        Node parent = this.root;
        int position = -1;

        while(current != null) {
            parent = current;
            if(x == current.getX() && y == current.getY()) {
                return current;
            }
            if(x < current.getX()) {
                if(y < current.getY()) {
                    current = current.getSingleChild(2);
                    position = 2;
                    //Ir para sudoeste
                } else {
                    current = current.getSingleChild(1);
                    position = 1;
                    //Ir para noroeste
                }
            } else {
                if(y < current.getY()) {
                    current = current.getSingleChild(3);
                    position = 3;
                    //Ir para sudeste
                } else {
                    current = current.getSingleChild(0);
                    position = 0;
                    //Ir para nordeste
                }
            }
        }
        return null;
    }

    public ArrayList<Integer> findCitiesInInterval(long[] first, long[] second){
        this.cities = new ArrayList<>();
        if(first[0] > second[0]){
            long aux = first[0];
            first[0] = second[0];
            second[0] = aux;
        }
        if(first[1] > second[1]){
            long aux = first[1];
            first[1] = second[1];
            second[1] = aux;
        }
        this.auxFindCitiesInInterval(this.root, first, second);

        return this.cities;
    }

    //Função que, dado um intervalo fechado de coordenadas,
    //encontra e retorna uma lista com os códigos das cidades dentro do intervalo
    private void auxFindCitiesInInterval(Node node, long[] first, long[] second){
        String call0 = "";
        String call1 = "";
        String call2 = "";
        String call3 = "";

        if(node == null) return;

        if(!(node.getX() >= first[0])){
            call1 += "x";
            call2 += "x";
        }
        if(!(node.getY() >= first[1])){
            call2 += "x";
            call3 += "x";
        }
        if(!(node.getX() <= second[0])){
            call0 += "x";
            call3 += "x";
        }
        if(!(node.getY() <= second[1])){
            call0 += "x";
            call1 += "x";
        }

        if(call0.equals("")){
            auxFindCitiesInInterval(node.getSingleChild(0), first, second);
        }
        if(call1.equals("")){
            auxFindCitiesInInterval(node.getSingleChild(1), first, second);
        }
        if(call2.equals("")){
            auxFindCitiesInInterval(node.getSingleChild(2), first, second);
        }
        if(call3.equals("")){
            auxFindCitiesInInterval(node.getSingleChild(3), first, second);
        }

        if(call0.equals("") && call1.equals("") && call2.equals("") && call3.equals("")){
            this.cities.add(node.getBcc().hashCode());
        }
    }

    public void updateListOfFields(){
        this.fields = new ArrayList<>();
        auxUpdateListOfFields(this.root);
    }

    public void auxUpdateListOfFields(Node node){
        this.fields.add(node.getField());

        if (node.getSingleChild(0) != null) {
            auxUpdateListOfFields(node.getSingleChild(0));
        }
        if (node.getSingleChild(1) != null) {
            auxUpdateListOfFields(node.getSingleChild(1));
        }
        if (node.getSingleChild(2) != null) {
            auxUpdateListOfFields(node.getSingleChild(2));
        }
        if (node.getSingleChild(3) != null) {
            auxUpdateListOfFields(node.getSingleChild(3));
        }
    }

    public void print() {
        auxPrint(this.root);
    }

    public void auxPrint(Node node) {

        System.out.println(node.getField() + " - X: " + node.getX() + ", Y: " + node.getY());

        if (node.getSingleChild(0) != null) {
            auxPrint(node.getSingleChild(0));
        }
        if (node.getSingleChild(1) != null) {
            auxPrint(node.getSingleChild(1));
        }
        if (node.getSingleChild(2) != null) {
            auxPrint(node.getSingleChild(2));
        }
        if (node.getSingleChild(3) != null) {
            auxPrint(node.getSingleChild(3));
        }
    }

    public ArrayList<String> getFields(){
        return this.fields;
    }
}
