package ds2.structures.btree;

import ds2.structures.tree.GenericNode;

public class Node implements GenericNode {
    private int numOfKeys = 0;
    private int[] keys;
    private Node[] children;
    private boolean leaf;

    public Node(int order) {
        this.numOfKeys = 0;
        this.keys = new int[2 * order - 1];
        this.children = new Node[2 * order];
        this.leaf = true;
    }

    public int getNumOfKeys() {
        return this.numOfKeys;
    }

    public void setNumOfKeys(int numOfKeys) {
        this.numOfKeys = numOfKeys;
    }

    public int getKey(int position) {
        return this.keys[position];
    }

    public void setKey(int id, int position) {
        this.keys[position] = id;
    }

    public Node getChild(int position) {
        return this.children[position];
    }

    public Node[] getChildren() {
        return this.children;
    }

    public void setChild(Node node, int position) {
        this.children[position] = node;
    }

    public boolean isLeaf() {
        return this.leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public int getValue(){
        return this.keys[0];
    }
}
