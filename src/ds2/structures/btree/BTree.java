package ds2.structures.btree;

import ds2.structures.tree.GenericTree;
import ds2.utils.Comparator;

import java.util.ArrayList;

public class BTree implements GenericTree<Node> {

    private int order;
    private Node root;
    private ArrayList<Integer> values;

    public BTree(int order) {
        this.order = order;
        this.root = new Node(this.order);
    }

    public void insert(int id) {
        Node rootNode = this.root;
        if (!getCorrectNode(this.root, id)) {
            if (rootNode.getNumOfKeys() == (2 * this.order - 1)) {
                Node newRootNode = new Node(this.order);
                this.root = newRootNode;
                newRootNode.setLeaf(false);
                this.root.setChild(rootNode, 0);
                splitChild(newRootNode, 0, rootNode);
                insertIntoNotFullNode(newRootNode, id);
            } else {
                insertIntoNotFullNode(rootNode, id);
            }
        }
    }

    //Dividir o nó passado como argumento e realocar as chaves dentro de seu vetor entre ele e seu irmao
    void splitChild(Node parentNode, int position, Node node) {
        Node newNode = new Node(this.order);
        newNode.setLeaf(node.isLeaf());
        newNode.setNumOfKeys(this.order - 1);
        for (int i = 0; i < this.order - 1; i++) {
            newNode.setKey(node.getKey(i + this.order), i);
        }
        if (!newNode.isLeaf()) {
            for (int j = 0; j < this.order; j++) { // Copy the last T pointers of node into newNode.
                newNode.setChild(node.getChild(j + this.order), j);
            }
            for (int j = this.order; j <= node.getNumOfKeys(); j++) {
                node.setChild(null, j);
            }
        }
        for (int j = this.order; j < node.getNumOfKeys(); j++) {
            node.setKey(0, j);
        }
        node.setNumOfKeys(this.order -1);

        for (int j = parentNode.getNumOfKeys(); j >= position + 1; j--) {
            parentNode.setChild(parentNode.getChild(j), j + 1);
        }
        parentNode.setChild(newNode, position + 1);
        for (int j = parentNode.getNumOfKeys() - 1; j >= position; j--) {
            parentNode.setKey(parentNode.getKey(j), j + 1);
        }
        parentNode.setKey(node.getKey(this.order - 1), position);
        node.setKey(0, this.order - 1);
        parentNode.setNumOfKeys(parentNode.getNumOfKeys() + 1);
    }

    void insertIntoNotFullNode(Node node, int id) {
        int i = node.getNumOfKeys() - 1;
        if (node.isLeaf()) {
            while (i >= 0 && id < node.getKey(i)) {
                node.setKey(node.getKey(i), i + 1);
                i--;
            }
            i++;
            node.setKey(id, i);
            node.setNumOfKeys(node.getNumOfKeys() + 1);
        } else {
            while (i >= 0 && id < node.getKey(i)) {
                i--;
            }
            i++;
            if (node.getChild(i).getNumOfKeys() == (2 * this.order - 1)) {
                splitChild(node, i, node.getChild(i));
                if (id > node.getKey(i)) {
                    i++;
                }
            }
            insertIntoNotFullNode(node.getChild(i), id);
        }
    }

    public Node search(int id, Comparator comparator) {
        return auxSearch(this.root, id, comparator);
    }

    private Node auxSearch(Node node, int id, Comparator comparator) {
        int i = 0;
        while (i < node.getNumOfKeys() && id > node.getKey(i)) {
            comparator.addComparisons();
            i++;
        }
        if (i < node.getNumOfKeys() && id == node.getKey(i)) {
            comparator.finishComparation();
            return node;
        }
        if (node.isLeaf()) {
            comparator.finishComparation();
            System.out.println("Item não encontrado na BTree");
            return null;
        } else {
            return auxSearch(node.getChild(i), id, comparator);
        }
    }

    public void updateListOfValues(){
        this.values = new ArrayList<>();
        auxUpdateListOfValues(this.root);
    }

    private void auxUpdateListOfValues(Node node) {
        if(node == null) {
            return;
        }
        for (int i = 0; i < node.getNumOfKeys(); i++) {
            this.values.add(node.getKey(i));
        }

        for(Node childNode: node.getChildren()) {
            auxUpdateListOfValues(childNode);
        }
    }

    private boolean getCorrectNode(Node node, int id) {
        while (node != null) {
            int i = 0;
            while (i < node.getNumOfKeys() && id > node.getKey(i)) {
                i++;
            }
            if (i < node.getNumOfKeys() && id == node.getKey(i)) {
                return true;
            }
            if (node.isLeaf()) {
                return false;
            } else {
                node = node.getChild(i);
            }
        }
        return false;
    }

    public void print() {
        auxPrint(this.root);
    }

    private void auxPrint(Node node) {
        if(node == null) {
            return;
        }
        for (int i = 0; i < node.getNumOfKeys(); i++) {
            System.out.println(node.getKey(i) + " ");
        }

        for(Node childNode: node.getChildren()) {
            auxPrint(childNode);
        }
    }

    public ArrayList<Integer> getValues(){
        return this.values;
    }

    public int getOrder(){
        return this.order;
    }
}